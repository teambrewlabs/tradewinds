import logging
import json

from ..sharedcode import request
from ..sharedcode import sqlwrapper

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    try:
        req_body = request.get_json(req)
    except ValueError:
        logging.info('Malformed request.')
        return func.HttpResponse(request.failure(2))
    else:
        auth = sqlwrapper.check_login(req_body)
        if auth is not None:
            return func.HttpResponse(request.failure(auth))
        #ports = req_body.get('ports')

    returnports = sqlwrapper.getports()
    return func.HttpResponse(request.success(dict(list=returnports)))

    #if ports:
    #    returnports = [dict(portid="thing", tileid="place", offers=[], production=dict(wood=0, coal=0, iron=20, gold=0), demand=dict(wood=10, coal=0, iron=20, gold=0), size=40)]
    #    return func.HttpResponse(request.success(dict(list=returnports)))
    #else:
    #    return func.HttpResponse(request.failure(3, "Missing parameters"))
