import json

def success(data):
    return json.dumps(dict(success=0, errormsg="", results=data))

def failure(errorcode, errormsg=None):
    errors=["Success", "Can't contact server", "Malformed JSON", "Missing parameters", "Incorrect credentials"]
    if errormsg is None:
        errormsg = errors[errorcode]
    return json.dumps(dict(success=errorcode, errormsg=errormsg))

def get_json(req):
    return json.loads(req.get_body().decode('utf-8'))
