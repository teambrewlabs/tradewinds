import logging
import json

from ..sharedcode import request
from ..sharedcode import sqlwrapper

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    nversion = sqlwrapper.getversion()
    return func.HttpResponse(request.success(dict(nversion=nversion)))
