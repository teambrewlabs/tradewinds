import requests
import json

commands = [
        ("login", dict(playerid="josh", password="12345")),
        ("getships", dict(players="test")),
        ("getversion", dict()),
        ("getports", dict(ports="test")),
        ("queueactions", dict(moves="test")),
        ("getmoves", dict(shipids="test")),
        ("getmoney", dict(players="test")),
        #("trade", dict())
        ("turntimer", dict()),
        ("nextturn", dict()),
        ("logout", dict()),
        ]

command = "login"
data = json.dumps(dict(playerid="josh", password="asdf"))
print("Running command "+command+" with data "+data)
r = requests.post("http://tradewindsctl.azurewebsites.net/api/"+command, data=data)
print(r.status_code, r.reason)
print(r.text)

playerid = "josh"
token = json.loads(r.text).get('results').get('token')

for (command, datadict) in commands:
    datadict['playerid'] = playerid
    datadict['token'] = token
    data = json.dumps(datadict)
    print("Running command "+command+" with data "+data)
    r = requests.post("http://tradewindsctl.azurewebsites.net/api/"+command, data=data)
    print(r.status_code, r.reason)
    print(r.text)

#data = json.dumps(dict(playerid="josh", password="12345"))
#data = '{"playerid": "test", "password": "12345"}'
#r = requests.post("http://tradewindsctl.azurewebsites.net/api/login", data=data)
#print(r.status_code, r.reason)
#print(r.text)
#        ("login", dict(playerid="josh", password="asdf")),
