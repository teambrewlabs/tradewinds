import logging
import json

from ..sharedcode import request

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    # todo processing
    return func.HttpResponse(request.success(dict()))
