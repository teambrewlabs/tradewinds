import logging
import json

from ..sharedcode import request
from ..sharedcode import sqlwrapper

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    try:
        req_body = request.get_json(req)
    except ValueError:
        logging.info('Malformed request.')
        return func.HttpResponse(request.failure(2))
    else:
        auth = sqlwrapper.check_login(req_body)
        if auth is not None:
            return func.HttpResponse(request.failure(auth))
        #players = req_body.get('players')

    return func.HttpResponse(request.success(dict(list=sqlwrapper.getmoney())))
    #if players:
    #    money = [dict(playerid="josh", balance=100)]
    #    return func.HttpResponse(request.success(dict(list=money)))
    #else:
    #    return func.HttpResponse(request.failure(3))
