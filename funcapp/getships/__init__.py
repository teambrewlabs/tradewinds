import logging
import json

from ..sharedcode import request
from ..sharedcode import sqlwrapper

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    try:
        req_body = request.get_json(req)
    except ValueError:
        logging.info('Malformed request.')
        return func.HttpResponse(request.failure(2))
    else:
        auth = sqlwrapper.check_login(req_body)
        if auth is not None:
            return func.HttpResponse(request.failure(auth))
        #players = req_body.get('players')

    ships = sqlwrapper.getships()
    return func.HttpResponse(request.success(dict(list=ships)))

    #if players:
    #    ships = [dict(playerid="josh", ships=[dict(shipid="boat", location="place", cargo=dict(wood=0, coal=9001, iron=0, gold=1), maxcargo=10)])]
    #    return func.HttpResponse(request.success(dict(list=ships)))
    #else:
    #    return func.HttpResponse(request.failure(3, "Missing parameters"))
