import logging
import json

from ..sharedcode import request

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    nextturn = 1
    currenttime = 1
    return func.HttpResponse(request.success(dict(nextturn=nextturn, currenttime=currenttime)))
