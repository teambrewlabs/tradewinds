import logging
import json

from ..sharedcode import request
from ..sharedcode import sqlwrapper

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    try:
        req_body = request.get_json(req)
    except ValueError:
        logging.info('Malformed request.')
        return func.HttpResponse(request.failure(2, "Malformed JSON"))
    else:
        auth = sqlwrapper.check_login(req_body)
        if auth is not None:
            return func.HttpResponse(request.failure(auth))
        shipid = req_body.get('shipid')
        moves = req_body.get('moves')

    if moves:
        sqlwrapper.queueactions(shipid, moves)
        return func.HttpResponse(request.success(dict()))
    else:
        return func.HttpResponse(request.failure(3, "Missing parameters"))

