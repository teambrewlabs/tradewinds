import logging
import json

from ..sharedcode import request
from ..sharedcode import sqlwrapper

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    try:
        req_body = request.get_json(req)
    except ValueError:
        logging.info('Malformed request.')
        return func.HttpResponse(request.failure(2))
    else:
        auth = sqlwrapper.check_login(req_body)
        if auth is not None:
            return func.HttpResponse(request.failure(auth))
        playerid = req_body.get('playerid')

    moves = sqlwrapper.getmoves(playerid)
    return func.HttpResponse(request.success(dict(list=moves)))

    #if shipids:
    #    moves = [dict(shipid="thing", moves=[dict(tileid="place")])]
    #    return func.HttpResponse(request.success(dict(list=moves)))
    #else:
    #    return func.HttpResponse(request.failure(3, "Missing parameters"))
