import pypyodbc
import random, string

server = 'tradewinds-sqlserver.database.windows.net'
database = 'tradewinds-db'
#username = 'sql'
#password = 'l2FGEg7kmUFi'
username = 'srv'
password = 'Security123'
driver= '{ODBC Driver 17 for SQL Server}'
connectstring = 'DRIVER='+driver+';SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+password

cnxn = pypyodbc.connect(connectstring)
cursor = cnxn.cursor()
#cursor.autocommit = True

def login(playerid, password):
    #try:
    #    cnxn = pypyodbc.connect(connectstring)
    #    cursor = cnxn.cursor()
    #except Exception as e:
    #    return connectstring + str(e)
    if playerid and password:
        cursor.execute("SELECT TOP 1 playerid FROM players WHERE playerid = '{}' AND password = '{}'".format(playerid, password))
        data = cursor.fetchone()
    else:
        return None
    if data:
        cursor.fetchall()
        token = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
        cursor.execute("UPDATE players SET token = '{}' WHERE playerid = '{}'".format(token, playerid))
        cursor.commit()
        return token
    else:
        return None

def check_login(req_body):
    playerid = req_body.get('playerid')
    token = req_body.get('token')

    if playerid and token:
        cursor.execute("SELECT TOP 1 playerid FROM players WHERE playerid = '{}' AND token = '{}'".format(playerid, token))
        data = cursor.fetchone()
        if data:
            cursor.fetchall()
            return None
        else:
            return 4 # Incorrect credentials
    else:
        return 3 # Missing parameters

def getmoney():
    cursor.execute("SELECT playerid, balance FROM players")
    moneys = []
    data = cursor.fetchone()
    while data:
        moneys.append(dict(playerid=data[0], balance=data[1]))
        data = cursor.fetchone()
    return moneys

def getversion():
    cursor.execute("SELECT version FROM globals")
    data = cursor.fetchone()
    return data[0]

def getships():
    ships = []
    players = []
    cursor.execute("SELECT playerid FROM players")
    data = cursor.fetchone()
    while data:
        players.append(data[0])
        data = cursor.fetchone()
    for playerid in players:
        player = dict(playerid=playerid, ships=[])
        cursor.execute("SELECT shipid, location, maxcargo FROM ships WHERE owner = '{}'".format(playerid))
        data = cursor.fetchone()
        while data:
            player['ships'].append(dict(shipid=data[0], location=data[1], maxcargo=data[2], cargo=dict()))
            data = cursor.fetchone()
        for i in range(len(player['ships'])):
            cursor.execute("SELECT resource, amount FROM cargo WHERE shipid = '{}'".format(player['ships'][i]['shipid']))
            data = cursor.fetchone()
            while data:
                player['ships'][i]['cargo'][data[0]] = data[1]
                data = cursor.fetchone()
        ships.append(player)
    return ships

def getports():
    ports = []
    cursor.execute("SELECT portid, location, size FROM ports")
    data = cursor.fetchone()
    while data:
        ports.append(dict(portid=data[0], tileid=data[1], size=data[2], production=dict(), demand=dict(), offers=[]))
        data = cursor.fetchone()
    # production/demand
    for i in range(len(ports)):
        cursor.execute("SELECT type, resource, amount FROM tradecapabilities WHERE portid = '{}'".format(ports[i]['portid']))
        data = cursor.fetchone()
        while data:
            if data[0] == "production":
                ports[i]['production'][data[1]] = data[2]
            else:
                ports[i]['demand'][data[1]] = data[2]
            data = cursor.fetchone()
    # offers
    return ports

def getmoves(playerid):
    ships = []
    cursor.execute("SELECT shipid FROM ships WHERE owner = '{}'".format(playerid))
    data = cursor.fetchone()
    while data:
        ships.append(dict(shipid=data[0], moves=[]))
        data = cursor.fetchone()
    for i in range(len(ships)):
        cursor.execute("SELECT turn_number, tileid FROM turns WHERE shipid = '{}' ORDER BY turn_number ASC".format(ships[i]['shipid']))
        data = cursor.fetchone()
        while data:
            ships[i]['moves'].append(data[2])
            data = cursor.fetchone()
    return ships

def queueactions(shipid, moves):
    cursor.execute("SELECT turn_number FROM global")
    data = cursor.fetchone()
    turn_number = data[0]
    for i in range(len(moves)):
        cursor.execute("INSERT INTO turns ([shipid], [turn_number], [tileid]) VALUES ('{}', {}, '{}')".format(shipid, turn_number+i+1, moves[i]))
        cursor.commit()

def moveship(shipid, tileid):
    cursor.execute("UPDATE ships SET tileid = '{}' WHERE shipid = '{}'".format(tileid, shipid))
    cursor.commit()
