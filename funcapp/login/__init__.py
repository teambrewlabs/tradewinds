import logging
import json, random, string

from ..sharedcode import request
from ..sharedcode import sqlwrapper

import azure.functions as func


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    try:
        req_body = request.get_json(req)
    except ValueError:
        logging.info('Malformed request.')
        return func.HttpResponse(request.failure(2, "Malformed JSON"))
    else:
        playerid = req_body.get('playerid')
        password = req_body.get('password')

    if playerid and password:
        try:
            token = sqlwrapper.login(playerid, password)
        except Exception as e:
            return func.HttpResponse(request.failure(5, str(e)))
        if token is not None:
            return func.HttpResponse(request.success(dict(token=token)))
        else:
            return func.HttpResponse(request.failure(4, "Incorrect credentials"))
        #token = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
        #return func.HttpResponse(request.success(dict(token=token)))
    else:
        return func.HttpResponse(request.failure(3, "Missing parameters"))
